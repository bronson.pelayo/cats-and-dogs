﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using CatsDogs.DTOs;
using Microsoft.Extensions.Configuration;
using RestSharp;

namespace CatsDogs.Services
{
    public class PetImageService
    {
        private IConfigurationSection _config; 
        private RestClient _client; 
        public PetImageService(IConfigurationSection config) 
        {
            _config = config;
            _client = new RestClient(_config.GetSection("Url").Value);
        }
        public async Task<Image> GetImage(string breedId) 
        {

            var request = new RestRequest("images/search", Method.Get);
            request.AddParameter("breed_id", breedId);
            request.AddHeader("x-api-key", _config.GetSection("Key").Value);
            RestResponse response = await _client.ExecuteAsync(request);
            
            if (response.IsSuccessful)
            {
                return  JsonSerializer.Deserialize<List<Image>>(response.Content).FirstOrDefault();
            }

            return new Image();
        }

        public async Task<List<Image>> FindImage(string breedId, Page paging)
        {
            var request = new RestRequest("images/search", Method.Get);
            request.AddParameter("breed_id", breedId);
            request.AddHeader("x-api-key", _config.GetSection("Key").Value);
            request.AddParameter("page", paging.page);
            request.AddParameter("limit", paging.limit);
            RestResponse response = await _client.ExecuteAsync(request);

            if (response.IsSuccessful)
            {
                return JsonSerializer.Deserialize<List<Image>>(response.Content);
            }

            return new List<Image>();
        }
    }
}
