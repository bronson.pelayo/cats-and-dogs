﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using CatsDogs.DTOs;
using Microsoft.Extensions.Configuration;
using RestSharp;

namespace CatsDogs.Services
{
    public class CatService
    {
        private IConfigurationSection _config; 
        private RestClient _client; 
        private PetImageService _imageService; 
        public CatService(IConfigurationSection config, PetImageService imageService) 
        {
            _config = config;
            _client = new RestClient(_config.GetSection("Url").Value);
            _imageService = imageService;
        }

        public async Task<List<Cat>> GetAllBreed(Page paging)
        {
            var request = new RestRequest("breeds", Method.Get);
            request.AddHeader("x-api-key", _config.GetSection("Key").Value);
            request.AddParameter("page", paging.page);
            request.AddParameter("limit", paging.limit);
            RestResponse response = await _client.ExecuteAsync(request);
            var retVal = new List<Cat>();

            if (response.IsSuccessful)
            {
                var cats = JsonSerializer.Deserialize<List<Cat>>(response.Content);
                if (cats.Count == 0) 
                {
                    return retVal;
                }
                                
                foreach (Cat cat in cats) 
                {
                    cat.image = await _imageService.GetImage(cat.id);
                    retVal.Add(cat);
                }
            }

            return retVal;
        }
    }
}
