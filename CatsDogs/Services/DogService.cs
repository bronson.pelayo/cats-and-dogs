﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using CatsDogs.DTOs;
using Microsoft.Extensions.Configuration;
using RestSharp;

namespace CatsDogs.Services
{
    public class DogService
    {
        private IConfigurationSection _config; 
        private RestClient _client; 
        private PetImageService _imageService;
        public DogService(IConfigurationSection config, PetImageService imageService) 
        {
            _config = config;
            _client = new RestClient(_config.GetSection("Url").Value);
            _imageService = imageService;
        }

        public async Task<List<Dog>> GetAllBreed(Page paging)
        {
            var request = new RestRequest("breeds", Method.Get);
            request.AddHeader("x-api-key", _config.GetSection("Key").Value);
            request.AddParameter("page", paging.page);
            request.AddParameter("limit", paging.limit);
            RestResponse response = await _client.ExecuteAsync(request);
            var retVal = new List<Dog>();

            if (response.IsSuccessful)
            {
                var dogs = JsonSerializer.Deserialize<List<Dog>>(response.Content);
                if (dogs.Count == 0) 
                {
                    return retVal;
                }
                                
                foreach (Dog dog in dogs) 
                {
                    dog.image = await _imageService.GetImage(dog.id.ToString());
                    retVal.Add(dog);
                }
            }

            return retVal;
        }
    }
}
