using System;
using Microsoft.AspNetCore.Mvc;

namespace CatsDogs
{
    [Route("api/[controller]")]
    [ApiVersion("1.0")]
    public class WeatherForecast
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string Summary { get; set; }
    }
}
