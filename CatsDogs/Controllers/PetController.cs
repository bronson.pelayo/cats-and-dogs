﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using CatsDogs.DTOs;
using CatsDogs.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace CatsDogs.Controllers
{
    [ApiVersion("1.0")]
    [Route("")]
    [Route("v{version:apiVersion}/")]
    [Route("v{version:apiVersion}/[controller]")]
    public class PetController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private PetImageService _dogImage;
        private PetImageService _catImage;

        public PetController(IConfiguration configuration)
        {
            _configuration = configuration;
            _dogImage = new PetImageService(_configuration.GetSection("CatDog").GetSection("DogApi"));
            _catImage = new PetImageService(_configuration.GetSection("CatDog").GetSection("CatApi"));
        }

        [Route("breeds")]
        [HttpGet]
        public async Task<IActionResult> GetBreed([FromQuery] Page paging)
        {

            var catService = new CatService(
                _configuration.GetSection("CatDog").GetSection("CatApi"), 
                _catImage
            );

            var dogService = new DogService(
                _configuration.GetSection("CatDog").GetSection("DogApi"),
                _dogImage
            );

            var dog = await dogService.GetAllBreed(paging);
            var cat = await catService.GetAllBreed(paging);

            var response  = new Response(paging);
            response.result.AddRange(dog);
            response.result.AddRange(cat);

            return Ok(response);
        }

        [Route("breeds/{breed?}")]
        [HttpGet]
        public async Task<IActionResult> GetImage([FromQuery] Page paging, string? breed) 
        {
            var response  = new Response(paging);
            var dog = await _dogImage.FindImage(breed, paging);
            var cat = await _catImage.FindImage(breed, paging);
            var result  = new Response(paging);
            response.result.AddRange(dog);
            response.result.AddRange(cat);
            return Ok(result);

        }
    }
}
