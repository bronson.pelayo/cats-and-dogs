﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatsDogs.DTOs
{
    public class Page
    {
        public int page { get; set; } = 0;
        public int limit { get; set; } = 10;

    }
}
