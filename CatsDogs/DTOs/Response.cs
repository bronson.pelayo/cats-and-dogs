﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatsDogs.DTOs
{
    public class Response
    {
        public int page { get; set; }
        public int limit { get; set; }
        public List<object> result { get; set; }

        public Response(Page paging) 
        {
            page = paging.page;
            limit = paging.limit;
            result = new List<object>();
        }
    }
}
