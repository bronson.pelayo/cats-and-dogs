﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CatsDogs.DTOs
{
    public class Pet
    {
        public string name { get; set; }
        public string temperament { get; set; }
        public string origin { get; set; }
        public string country_code { get; set; }
        public string description { get; set; }
        public string wikipedia_url { get; set; }
        public object image { get; set; }

        [JsonPropertyName("name2")]
        private string weight_imperial { set { temperament = value; } }
    }

    public class Image
    {
        public string id { get; set; }
        public string url { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Dog : Pet
    {
        public int id { get; set; }
    }

    public class Cat : Pet
    {
        public string id { get; set; }
    }
}
